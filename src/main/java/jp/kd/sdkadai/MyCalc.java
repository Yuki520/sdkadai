package jp.kd.sdkadai;

import java.math.BigInteger;

public class MyCalc {
	private int num = 0;

	public Integer getNum() {
		return num;
	}

	public void setNum(int i) {
		num = i;
	}

	public Integer ex1(int i) {
		i = Math.abs(i)*num;
		return i;
	}

	public Boolean ex2() {
		Boolean flg=true;
		if(num<=1){
			flg=false;
		}else{
			for(int i=2;i<num;i++){
				if(num%i==0){
					flg=false;
					break;
				}
			}
		}
		return flg;
		
	}

	public String ex3(Integer num2) {
		 BigInteger kiso = BigInteger.valueOf(num2);
		 BigInteger zyou = BigInteger.valueOf(num2);
		for (int i=2;i<=num2;i++){
			kiso = kiso.multiply(zyou);
		}
		return String.valueOf(kiso);
	}

	
}
